<link 
    href="style-readme.css" 
    rel="stylesheet" 
    type="text/css"
    media="all" 
/>

<img src='logo_uem.png' class="aw">

# Física Experimental I 🔬

Esse repositório tem o intuíto de compartilhar o conteúdo criado para ministrar a disciplina de **Física Experimental I**, ministrada na [Universidade Estadual de Maringá (UEM)](http://www.uem.br/). O material foi criado para que as aulas pudessem ser ministradas online durante o regime de **Ensino Remoto Emergencial (ERE)**, devido a pandemia causada pelo novo coronavírus.

# Ementa 📄

Essa ementa segue a apostila de [Manual de Laboratório de Física Experimental I](https://www.google.com/url?q=http://site.dfi.uem.br/wp-content/uploads/2018/04/Manual-de-Laborat%25C3%25B3rio-de-F%25C3%25ADsica-Experimental-I.pdf&sa=D&source=calendar&usd=2&usg=AOvVaw1ywSpGtofN6hz3vH4PtKc1).

Esse é um curso semestral com carga horária de 34 horas. 

### Medidas e Noções sobre a Teoria de Erros 📏
- Erros. 
- Instrumentação de medidas.
- Erros de observação.
- Erros sistemáticos e erro instrumental.
- Propagação de erros.

### Experimentos 🧪
- Equações da cinemática.
- Plano inclinado.
- Leis de Newton.
- Movimento circular.
- Leis de conservação.
- Colisões: conservação do momento linear e energia cinética.
- Momento de inércia.

### Objetivos 🎯

Oferecer uma formação básica em Mecânica Clássica via experimentos.

# Conteúdo disponível 🗂

- 📂 **cap_3-mru**: experimento do Movimento Retilíneo Uniforme (MRU)
   - 🎥 **mru.mp4**
        Vídeo explicando o experimento do (MRU)
   - 📉 **mru.xlsx**
        `Arquivo excel` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **cap_4-mruv**: experimento do Movimento Retilíneo Uniformente Variável (MRUV)
    - 🎥 **mruv.mp4**
        Vídeo explicando o experimento do (MRU)
   - 📉 **mruv.xlsx**
        `Arquivo excel` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **cap_5-leis_de_newton**: experimento das Leis de Newton
    - 🎥 **leis_de_newton.mp4**
        Vídeo explicando o experimento das Leis de Newton
   - 📉 **leis_de_newton.docx**
        `Arquivo word` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **cap_6-mcu**: experimento do Movimento Circular Uniforme (MCU)
    - 🎥 **mcu.mp4**
        Vídeo explicando o experimento do (MCU)
   - 📉 **mcu.pdf**
        `Arquivo pdf` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **cap_7-colisoes**: experimento sobre colisões
    - 🎥 **colisoes_elasticas.mp4**
        Vídeo explicando o experimento de colisões elásticas
    - 🎥 **colisoes_inelasticas.mp4**
        Vídeo explicando o experimento de colisões inelásticas
   - 📉 **colisoes.pdf**
        `Arquivo pdf` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **cap_8-colisoes**: experimento sobre colisões
    - 🎥 [Capítulo 8 - Momento de Inércia (YouTube)](https://www.youtube.com/watch?v=vsYb-elWvvs)
    - 📉 **momento_de_inercia.xlsx**
        `Arquivo excel` contendo os dados do experimento realizado e a análise com os gráficos e resultados.
<br>

- 📂 **papeis**: pasta com papeis com divisoras usados para confeccionar gráficos no laboratório
    - 📉 **milimetrado.pdf**
        Papel com divisões lineares, em milimetros (mm), na horizontal e na vertical.
    - 📉 **mono-log.pdf**
        Papel com divisões linear (mm) e logaritmica na horizontal e na vertical, respectivamente.
    - 📉 **di-log.pdf**
        Papel com divisões logaritmicas na horizontal e na vertical.


# Outros links 🔗

- [Paquímetro 🔧](https://www.stefanelli.eng.br/paquimetro-virtual-simulador-milimetro-05/#swiffycontainer_1)<br>
    Site que simula o comportamento de um paquímetro.

- [Curso de extensão 📚](https://github.com/vieirads/Curso-de-extensao)<br>
    Aprenda como usar a linguagem de programação `python` para realizar as análises de dados nessa disciplina.

- [Simulações em física (EN)](https://www.myphysicslab.com/)<br>
    Site com simulação de diversos sistemas físicos.

- [GeoGebra](https://www.geogebra.org/?lang=pt)<br>
    Aplicativos matemáticos com acesso livre para gráficos, geometria, 3D e muito mais.

# Obtendo o Repositório 📩

<!-- <p>
Você precisará do repositório para usar o <mark class='mono_gray'>Jupyter Notebook</mark> contendo os conteúdos das aulas. 
</p> -->

<p>
Para obter os arquivos do curso, basta clicar aqui: <a href='https://gitlab.com/vieirads/fisica-experimental-i/-/archive/master/fisica-experimental-i-master.zip' target='_blank'>Baixar repositório</a>. Você irá obter um arquivo chamado <mark class="mono_gray">fisica-experimental-I.zip</mark>. Se o link não funcionar, entre no <a href='https://gitlab.com/vieirads/fisica-experimental-i' target='_blank'>repositório do GitLab</a> e clique no botão de download e em seguida clique em <mark class="white">Download ZIP</mark> (<b>Figura 1</b>).
</p>

<figure align='center'>
    <img src='download.png' alt='Instruções para baixar o repositório.' class="aw" style="width: 400px">
    <figcaption><b>Fig. 1:</b> Fazendo o download dos arquivos.</figcaption>
</figure>

<p>
Salve o arquivo em um local que for mais apropriado e descompacte-o. 
</p>

# Contribuidores 🤝

- Profa. Dra. Aline Alves Oliveira (aaoliveira3@uem.br)
- Prof. Dr. Denner Serafim Vieira (dsvieira2@uem.br)
- Prof. Dr. Felipe de Aguiar (fmaguiar2@uem.br)
- Prof. Dr. Guilherme Maia Santos (gmsantos2@uem.br)
- Profa. Dra. Hatsumi Mukai (hmukai@uem.br)
- Prof. Dr. Leandro de Santana Costa (lscosta2@uem.br)
- Prof. Dr. Marlon Ivan Valerio Cuadros (mivcuadros@uem.br)
- Prof. Dr. Omar Cleo Neves Pereira (ocnpereira2@uem.br)
- Prof. Dr. Otávio Augusto Capeloto (oacapeloto@ufam.edu.br)
- Prof. Dr. Otávio Augusto Protzek (oaprotzek2@uem.br)
- Prof. Dr. Reginaldo Barco (rbarco2@uem.br)
- Prof. Dr. Thiago Petrucci Rodrigues (tprodrigues2@uem.br)